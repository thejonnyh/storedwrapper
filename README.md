# StoredWrapper #

StoredWrapper has propertyWrappers that allow you to easily store CoreData objects with features like auto-save.

# Requirements
* iOS 14 or 
* macOS 11

# Installation
There are two ways to add to your own project
1. Add as a Swift Package Dependency 
2. Import this project into your Xcode project and add under Frameworks and Libraries. 

````http
https://bitbucket.org/thejonnyh/storedwrapper/src/main/
````

# Use
The StoredWrapper framework allows you to access the Stored & CoreStorage property wrappers.
Simply import StoredWrapper, then use as seen below. 

````swift
import StoredWrapper
````
````
@Stored(object: <NSManagedObject>) private var object
````
or 
````
@CoreStorage<NSManagedObject>(with: NSPredicate?, sorters: [NSSortDescriptors]) private var objects
````
You can now read from either the individual object or the group of objects that you specify. And, because both will look in the environment and use the viewContext available, it's that easy. 

### Contribution guidelines ###

Feel free to update and fork to improve the code. 

### Who do I talk to? ###

Jonathan Holland